# -*- coding: utf-8 -*
from django.contrib.auth import get_user_model

from rest_framework.serializers import ModelSerializer, SerializerMethodField


class UserDetailsSerializer(ModelSerializer):

    tw_screen_name = SerializerMethodField()

    class Meta:
        model = get_user_model()
        fields = ('username', 'tw_screen_name')

    def get_tw_screen_name(self, obj):
        sa_data = obj.social_auth.all().get(provider="twitter")
        return sa_data.extra_data['access_token']['screen_name']
