# -*- coding: utf-8 -*
import tweepy
import redis
import json
import threading

from django.conf import settings

from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework.decorators import detail_route


def cache_tweets_request(fn):

    def wrapper(class_instance, *args, **kwargs):
        r = redis.StrictRedis(db=settings.REDIS_DB_NUM)
        keys_list = [fn.__name__]
        for item in list(args) + kwargs.values():
            keys_list.append(str(item))
        key = "_".join(keys_list)
        cached_resp = r.get(key)
        if cached_resp:
            return tweepy.models.Status.parse_list(class_instance.api,
                                                   json.loads(cached_resp))
        resp = fn(class_instance, *args, **kwargs)
        result = []
        for item in resp:
            result.append(item._json)
        r.setex(key, 900, json.dumps(result))
        return resp

    return wrapper


class TwApi(object):
    """
    Helper class for auth tweepy through access_token saved
    in request.user.social_auth table
    """
    def __init__(self, user):
        auth = tweepy.OAuthHandler(settings.SOCIAL_AUTH_TWITTER_KEY,
                                   settings.SOCIAL_AUTH_TWITTER_SECRET)
        user_data = user.social_auth.get(provider="twitter").extra_data
        auth.set_access_token(user_data['access_token']['oauth_token'],
                              user_data['access_token']['oauth_token_secret'])

        self.api = tweepy.API(auth)

    def get_oembed_video(self, tweet_id):
        embed = self.api.get_oembed(id=tweet_id, widget_type="video")
        return embed['html']

    def get_user(self, username):
        return self.api.get_user(screen_name=username)

    def search_users(self, *args):
        return self.api.search_users(*args)

    @cache_tweets_request
    def get_user_tweets(self, **kwargs):
        return self.api.user_timeline(**kwargs)


class TwMediaFilter(object):
    """
    Helper class for filtering tweets by media content
    """
    def __init__(self, condition, tweets):
        if condition in ['any', 'image', 'url', 'video']:
            self.condition = condition
        else:
            self.condition = None
        self.tweets = tweets

    def has_media(self, tweet):
        if len(tweet.entities.get('media', [])) > 0 or \
                len(tweet.entities.get('urls', [])) > 0:
            return True
        return False

    def has_img(self, tweet):
        if hasattr(tweet, 'extended_entities'):
            for media_item in tweet.extended_entities.get('media', []):
                if media_item['type'] == 'photo':
                    return True
        return False

    def has_video(self, tweet):
        if hasattr(tweet, 'extended_entities'):
            for media_item in tweet.extended_entities.get('media', []):
                    if media_item['type'] in ['video', 'animated_gif']:
                        return True
        return False

    def has_url(self, tweet):
        if len(tweet.entities.get('urls', [])) > 0:
            return True
        return False

    @property
    def filtered_tweets(self):
        for tweet in self.tweets:
            ext_ent = []
            if hasattr(tweet, 'extended_entities'):
                ext_ent = tweet.extended_entities
            item = {'id': tweet.id_str,
                    'created_at': tweet.created_at,
                    'entities': tweet.entities,
                    'extended_enitities': ext_ent,
                    'text': tweet.text,
                    'parsed_text': ''}
            if not self.condition or \
                    self.condition == 'any' and self.has_media(tweet) or \
                    self.condition == 'image' and self.has_img(tweet) or \
                    self.condition == 'video' and self.has_video(tweet) or \
                    self.condition == 'url' and self.has_url(tweet):
                yield item


class PreventiveCallThread(threading.Thread):

    def __init__(self, user, uid, condition, max_id, next_max_id, **kwargs):
        self.user = user
        self.uid = uid
        self.condition = condition
        self.max_id = max_id
        self.next_max_id = next_max_id
        super(PreventiveCallThread, self).__init__(**kwargs)

    def run(self):
        if not self.condition and not self.max_id:
            get_tweets(self.user, self.uid, self.condition, self.max_id, True)
            for cnd in ['any', 'image', 'video', 'url']:
                get_tweets(self.user, self.uid, cnd, None, True)
        else:
            get_tweets(self.user, self.uid, self.condition,
                       self.next_max_id, True)


def do_preventive_call(fn):

    def wrapper(user, uid, condition, max_id, preventive=False):
        resp = fn(user, uid, condition, max_id, preventive)

        if not preventive and not resp['eot']:
            PreventiveCallThread(user, uid, condition,
                                 max_id, resp['last']).start()
        return resp

    return wrapper


@do_preventive_call
def get_tweets(user, uid, condition, max_id, preventive=False):
    api = TwApi(user)
    result = []
    eot = False  # end of timeline
    count = 21
    request_count = 0
    while len(result) < 20:
        tweets = api.get_user_tweets(screen_name=uid,
                                     max_id=max_id,
                                     count=count)
        request_count += 1
        if len(tweets) == 0:
            return {'eot': True, 'last': max_id, 'items': result}
        max_id = tweets[-1].id_str
        media_filter = TwMediaFilter(condition, tweets[:-1])
        for item in media_filter.filtered_tweets:
            if len(result) < 20:
                result.append(item)
        if len(tweets) < count:
            eot = True
            break
        if request_count > 5:
            count = 201

    return {'eot': eot, 'last': max_id, 'items': result}


class UsersViewSet(ViewSet):

    lookup_field = 'uid'
    lookup_value_regex = '[0-9a-zA-Z_]{1,15}'

    def retrieve(self, request, uid=None):
        api = TwApi(request.user)
        user = api.get_user(uid)
        image_big = user.profile_image_url.replace('normal.', '400x400.')
        item = {'id': user.id,
                'screen_name': user.screen_name,
                'profile_image_url': user.profile_image_url,
                'profile_image_big': image_big,
                'name': user.name}
        return Response(item)

    @detail_route()
    def tweets(self, request, uid):
        max_id = request.query_params.get('last', None)
        filter_condition = request.query_params.get('media', None)
        result = get_tweets(request.user, uid, filter_condition, max_id)
        return Response(result)

    def get_embed_video(self, request, username, tid):
        api = TwApi(request.user)
        return Response({'oembed_code': api.get_oembed_video(tid)})

    def list(self, request):
        q = request.query_params.get('q', None)
        page = request.query_params.get('page', None)
        if not q:
            return Response([])
        api = TwApi(request.user)

        users = api.search_users(q, 10, page)
        result = []

        for user in users:
            item = {'id': user.id,
                    'screen_name': user.screen_name,
                    'profile_image_url': user.profile_image_url,
                    'name': user.name}
            result.append(item)

        return Response(result)
