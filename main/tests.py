# -*- coding: utf-8 -*
import redis

from django.conf import settings
from django.test import LiveServerTestCase
from django.contrib.auth.models import User
from django.test.utils import override_settings

from social.apps.django_app.default.models import UserSocialAuth


@override_settings(REDIS_DB_NUM=2)
class TwTestCase(LiveServerTestCase):

    def setUp(self):
        self.user_ivan = User.objects.create_user(username='Ivan',
                                                  email='ivan@…',
                                                  password='password')

        tw_extra_data = {
            'access_token': settings.TEST_TWITTER_TOKEN,
            'access_token_secret': settings.TEST_TWITTER_TOKEN_SECRET}

        UserSocialAuth.objects.create(user=self.user_ivan, provider="twitter",
                                      uid="IvanPomidorov",
                                      extra_data=tw_extra_data)

        r = redis.StrictRedis(db=settings.REDIS_DB_NUM)
        r.flushdb()
        self.client.login(username="Ivan", password="password")

    def test_recieve_users(self):
        resp = self.client.get('/api/users/', {'q': u'Barack Obama'})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.data), 20)
        resp2 = self.client.get('/api/users/', {'q': u'Barack Obama',
                                                'page': u'2'})
        self.assertEqual(resp.status_code, 200)
        self.assertNotEqual(resp.data, resp2.data)

    def test_recieve_tweets(self):
        resp = self.client.get('/api/users/KremlinRussia/tweets/')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.data['items']), 20)
        last_tweet_id = resp.data['last']
        self.assertFalse(resp.data['eot'])
        resp2 = self.client.get('/api/users/KremlinRussia/tweets/',
                                {'last': last_tweet_id})
        self.assertEqual(len(resp2.data['items']), 20)
        self.assertEqual(resp2.data['items'][0]['id'], last_tweet_id)

    def test_filtered_tweets(self):
        resp = self.client.get('/api/users/ALyisenkov/tweets/',
                               {'media': 'image'})
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.data['eot'])
        self.assertEqual(len(resp.data['items']), 2)
        resp = self.client.get('/api/users/ALyisenkov/tweets/',
                               {'media': 'video'})
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.data['eot'])
        self.assertEqual(len(resp.data['items']), 1)

        resp = self.client.get('/api/users/ALyisenkov/tweets/',
                               {'media': 'any'})
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.data['eot'])
        self.assertEqual(len(resp.data['items']), 3)
