# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

from os.path import dirname, abspath

DJANGO_ROOT = dirname(dirname(abspath(__file__)))

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'twdb',
        'USER': 'tw',
        'PASSWORD': 'tw',
        'HOST': 'localhost',
        'PORT': '',
        'TEST': {'NAME': 'twtest'}
    }
}

TORNADO_PORT = 8089
TORNADO_HOST = "127.0.0.1"
