#####################################################
# For setting local environment do:
# Open your env/bin/activate and append the following
# code at the end of file
# IS_LOCAL_ENV="true"
# export IS_LOCAL_ENV
#####################################################


import os
from common import *

IS_LOCAL_ENV = 'IS_LOCAL_ENV' in os.environ

if IS_LOCAL_ENV:
    from development import *
else:
    from production import *
